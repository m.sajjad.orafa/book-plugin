<?php
global $wpdb, $charset_collate; 
$table = $wpdb->prefix . "books_info"; 
    
$query = "CREATE TABLE $table ( 
ID int(11) NOT NULL AUTO_INCREMENT, 
post_id int(11) NOT NULL, 
isbn int(11) NOT NULL, 
PRIMARY KEY (ID));"; 
    
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' ); 
dbDelta( $query ); 