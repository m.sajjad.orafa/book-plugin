<?php

add_action( 'save_post', 'add_book_fields', 10, 2 );
function add_book_fields( $book_id, $book ) {
	global $wpdb;
    if ( $book->post_type == 'book' ) {
        if ( isset( $_POST['meta'] ) ) {
            foreach( $_POST['meta'] as $key => $value ){
				update_post_meta( $book_id, $key, $value );
				$wpdb->insert( $wpdb->prefix . 'books_info', 
					array( 
					'post_id' =>  $book_id ,
					'isbn' =>  $value)
					,
					array( 
					'%d',
					'%d') );
			}
		}
    }
}