<?php
function book_posttype() {
	$labels = array( 'name' => __('Books') );
	$args = array(
		'labels' => $labels,
		'supports' => array('title', 'editor', 'thumbnail'),
		'taxonomies' => array('book_tax'), 'public' => true, 'menu_icon' => 'dashicons-book'
	);
	register_post_type('book', $args);
}
add_action('init', 'book_posttype', 0);

add_action( 'admin_init', 'my_admin_book' );
function my_admin_book() {
    add_meta_box( 'book_meta_box', 'ISBN', 'display_book_meta_box','book', 'normal', 'high' );
}
function display_book_meta_box( $book ) {?>
	<input type="text" style="width:425px;" name="meta[payment]" value="<?php echo esc_html( get_post_meta( $book->ID, 'payment', true ) );?>" />
            
<?php 
}
