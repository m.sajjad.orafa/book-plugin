<?php
function authors_tax() {
	$labels = array('name' => __('authors'));
	$args = array( 'labels' => $labels, 'hierarchical' => true );
	register_taxonomy( 'ads_tax', array('book'), $args );
}
add_action('init', 'authors_tax', 0);

function publisher_tax() {
	$labels = array('name' => __('publisher'));
	$args = array( 'labels' => $labels, 'hierarchical' => true );
	register_taxonomy( 'Publisher', array('book'), $args );
}
add_action('init', 'publisher_tax', 0);
