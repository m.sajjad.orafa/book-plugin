<?php
/*
 * Plugin Name: Books-Plugin
 * Version: 1
 * Plugin URI: http://sajadorafa76.ir/
 * Description: افزونه کتابخانه
 * Author: Mohammad Sajjad Orafa
 * Author URI: http://sajadorafa76.ir/
 * Text Domain: book
 */
defined('ABSPATH') || exit('No Direct Access!!');
define('GTGLOBAL_DIR',plugin_dir_path(__FILE__));
define('GTGLOBAL_URL',plugin_dir_URL(__FILE__));
define('GTGLOBAL_CSS_URL', trailingslashit(GTGLOBAL_URL.'assets/css'));
define('GTGLOBAL_JS_URL', trailingslashit(GTGLOBAL_URL.'assets/js'));
define('GTGLOBAL_IMG_URL', trailingslashit(GTGLOBAL_URL.'assets/img'));
define('GTGLOBAL_INC', trailingslashit(GTGLOBAL_DIR.'include'));
define('GTGLOBAL_ADMIN_DIR', trailingslashit(GTGLOBAL_DIR.'admin'));
require_once( ABSPATH . "wp-includes/pluggable.php" );

include GTGLOBAL_INC . "/global.php";
include GTGLOBAL_INC . "/functions.php";


include GTGLOBAL_INC . "/taxonomy/all.php";
include GTGLOBAL_INC . "/post_type/all.php";


include GTGLOBAL_INC . "/taxonomy/term-meta.php";


function gtglobal_add_theme_scripts() {
	wp_enqueue_style('global-style', GTGLOBAL_CSS_URL.'/style.css');
	wp_enqueue_script('global-script', GTGLOBAL_JS_URL. 'global.js', array ('jquery'));
	wp_enqueue_script('gt-script', GTGLOBAL_JS_URL. 'script.js', array ('jquery'));
}
add_action( 'wp_enqueue_scripts', 'gtglobal_add_theme_scripts' );

